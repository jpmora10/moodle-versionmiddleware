<?php
	
//Rutas para peliculas
Route::resource('movie','MovieController');
Route::post('movie/store', ['as' => 'movie/store', 'uses' => 'MovieController@store']);
Route::get('movie/destroy/{id}', ['as' => 'movie/destroy', 'uses' => 'MovieController@destroy']);
Route::put('movie/update', ['as' => 'movie/update', 'uses' => 'MovieController@update']);
Route::post('movie/search', ['as' => 'movie/search', 'uses' => 'MovieController@search']);

// Rutas para categorias
Route::resource('category','CategoryController');
Route::get('category/destroy/{id}', ['as' => 'category/destroy', 'uses' => 'CategoryController@destroy']);
Route::post('category/show', ['as' => 'category/show', 'uses' => 'CategoryController@show']);
Route::post('category/store', ['as' => 'category/store', 'uses' => 'CategoryController@store']);
Route::put('category/update', ['as' => 'category/update', 'uses' => 'CategoryController@update']);
Route::post('category/search', ['as' => 'category/search', 'uses' => 'CategoryController@search']);

// Rutas para Usuarios
Route::resource('user','UserController');
Route::get('user/destroy/{id}', ['as' => 'user/destroy', 'uses' => 'UserController@destroy']);
Route::post('user/search', ['as' => 'user/search', 'uses' => 'UserController@search']);

// Rutas para Estados
Route::resource('state','StateController');
Route::get('state/destroy/{id}', ['as' => 'state/destroy', 'uses' => 'StateController@destroy']);
Route::post('state/search', ['as' => 'state/search', 'uses' => 'StateController@search']);

// Rutas para Excel "Carga masiva"
Route::post('/import-excel', ['as'=> 'import-excel', 'uses' => 'ExcelController@importMovies']);

?>