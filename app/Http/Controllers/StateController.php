<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;

class StateController extends Controller
{
    public function index(){
    	$states = State::all();
    	return \View::make('states/list',compact('states'));
    }

    public function create(){
    	return \View::make('states/new');
    }

    public function store(Request $request){
    	$state = new State;
    	$state->state = $request->state;
    	$state->save();
    	return redirect('state');
    }

    public function edit($id){
        $state = State::find($id);
        return \View::make('states/update',compact('state'));
    }

    public function update($id, Request $request){
        $state = State::find($id);
        $state->state = $request->state;
        $state->save();
        return redirect('state');
    }

     public function search(Request $request){
        $states = State::where('state','like','%'.$request->state.'%')->get(); //busca por nombre similar
        $states = State::where('state','=',$request->state)->get();//busca por nombre igual
        return \View::make('states/list',compact('states'));
    }

    public function destroy($id) {
        $state = State::find($id);
        $state->delete();
        return redirect()->back();
    }
}
