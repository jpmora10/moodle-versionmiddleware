<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category as Category;
use App\Models\State;

class CategoryController extends Controller
{
	public function index() {
        $categories = Category::all();
        return \View::make('categories/list',compact('categories'));
    }

    public function store(Request $request){
    	$category = new Category;
        $category->name = $request->name;
        $category->state_id = 1;        
        $category->save();
        return;
    }

    public function create(){
    	return \View::make('categories/new');
    }

    public function edit($id) {
        $category = Category::find($id);
        $states = State::select('id','state')->get();
        return \View::make('categories/update', compact('category','states'));
    }

    public function update($id, Request $request) {
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->state_id = $request->state;        
        $category->save();
        return;
    }

     public function search(Request $request){
        $categories = Category::where('name','like','%'.$request->name.'%')->get(); //busca por nombre similar
        $categories = Category::where('name','=',$request->name)->get();//busca por nombre igual
        return \View::make('categories/list',compact('categories'));
    }

    public function destroy($id){
        $category = Category::find($id);
        $category->delete();
        return redirect()->back();
    }

}
