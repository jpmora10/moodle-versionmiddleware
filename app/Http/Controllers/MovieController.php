<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Movie;
use App\Models\State;

class MovieController extends Controller
{
    //Método cargar Lista de Peliculas
    public function index() {
        $movies = Movie::all();
        return \View::make('movies/list',compact('movies'));
    }


	//Método para cargar vista New en la carpeta movies
	public function create() {
        $users = DB::table('users')->get();
		return \View::make('movies/new',compact('users'));
	}

	//Método Guardar Movie
    public function store(Request $request) {
    	$movie = new Movie;
    	$movie->name = $request->name;
    	$movie->description = $request->description;
        $movie->state_id =1;
        $movie->user_id = \Auth::user()->id;
    	$movie->save();
    	return;
    }

    //Método consultar pelicula
    public function edit($id) {
        $movie = Movie::find($id);
        $states = State::select('id','state')->get();
        return \View::make('movies/update', compact('movie','states'));
    }

    // Método editar pelicula
    public function update(Request $request) {
        $movie = Movie::find($request->id);
        $movie->name = $request->name;
        $movie->description = $request->description;
        $movie->state_id = $request->state;
        $movie->save();
        return;
    }

    // Método de buscar pelicula
    public function search(Request $request){
        $movies = Movie::where('name','like','%'.$request->name.'%')->get(); //busca por nombre similar
        $movies = Movie::where('name','=',$request->name)->get();//busca por nombre igual
        return \View::make('movies/list',compact('movies'));
    }

    //  Método de eliminar pelicula
    public function destroy($id) {
        $movie = Movie::find($id);
        $movie->delete();
        return redirect()->back();
    }

    public function list(){
        $movies = Movie::all();
        return \View::make('movies/listMovies',compact('movies'));
    }
}
