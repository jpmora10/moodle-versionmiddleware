<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
class UserController extends Controller
{
    public function index(){
    	$users = User::all();
    	return \View::make('users/list',compact('users'));
    }

     public function search(Request $request){
        $users = User::where('name','like','%'.$request->name.'%')->get(); //busca por nombre similar
        //$users = User::where('name','=',$request->name)->get();//busca por nombre igual
        return \View::make('users/list',compact('users'));
    }

    public function edit($id){
    	$user = User::find($id);
        $states = DB::table('states')->get();
    	return \View::make('users/update', compact('user'),compact('states'));
    }

    public 	function update($id, Request $request){
    	$user = User::find($id);
    	$user->name = $request->name;
    	$user->email = $request->email;
        $user->role = $request->role;
        $user->state_id = $request->state;
    	$user->save();
    	return redirect('user');
    }

}
