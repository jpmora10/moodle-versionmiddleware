<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;


class ExcelController extends Controller
{
 public function importMovies(Request $request)
{
    \Excel::load($request->excel, function($reader) {
 
        $excel = $reader->get();
 
        // iteracción
        $reader->each(function($row) {
 			
            $movie = new Movie;
            $movie->name = $rowC->name;
            $movie->description = $row->description;
            $movie->user_id = $row->user_id;
            $movie->state_id = $row->state_id;
            $movie->save();
             
        });
    
    });
 
    return \View::make('movies/list',compact('movies'));
}
}
	