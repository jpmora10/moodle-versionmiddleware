<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	protected $table = 'states';
	protected $fillable = ['state'];
	protected $guarded = ['id'];

	public function categories(){

		return $this->hasMany('App/Models/Category');
	}

	public function users()
    {
    	return $this->hasMany('App/User');
    }
    
    // hasmany: muchos
    public function movies()
    {
    	return $this->hasMany('App\Models\Movie');
    }
}
