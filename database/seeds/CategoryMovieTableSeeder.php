<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoryMovieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 $faker = Faker::create();
    	 for ($i=0; $i < 30; $i++) {
	        \DB::table('category_movie')->insert(
				['movie_id' => $faker->numberBetween($min = 1, $max = 10), 
				'category_id' =>$faker->numberBetween($min = 1, $max = 10), 
				'state_id' => 1,
			]);
    	 }
			
    }
}
