<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Category;


class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        	$faker = Faker::create();
        	for ($i=0; $i < 11; $i++) { 
        	   $category = new Category;
        	   $category->name = $faker->randomElement($array = array ('Acción','Animación','Drama','Documental','Ciencia Ficción','Comedia','Infantil','Superheroes','Suspenso','Terror','+18'));
        	   $category->state_id = 1;
        	   $category->save();
        	}
    }
}