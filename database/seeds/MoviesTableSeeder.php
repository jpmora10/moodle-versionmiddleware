<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $faker = Faker::create();
       
        for ($i=0; $i < 10 ; $i++) { 
        	$movie = new Movie;
            $movie->name = $faker->name; 
            $movie->description = $faker->sentence($nbWords = 6, $variableNbWords = true);
            $movie->user_id = $faker->numberBetween($min = 1, $max = 10);
            $movie->state_id = 1;
            $movie->save();
        }
    }
}