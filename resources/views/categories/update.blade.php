@extends('layouts.app')
@section('content')
	<new-category :edit="true" :category="{{$category}}" :states="{{$states}}"></new-category>
@endsection