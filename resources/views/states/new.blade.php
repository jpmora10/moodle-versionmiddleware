@extends('layouts.app')
@section('content')
<?php


session_start();


if(isset($_POST['submit'])){


   require 'config.php';


   $insertOneResult = $collection->insertOne([
       'state' => $_POST['state'],
   ]);


   $_SESSION['success'] = "Estado registrado exitosamente";
   header("Location: list.blade.php");
}
?>
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::open(['route' => 'state.store', 'method' => 'post', 'novalidate']) !!}
					<div class="form-group">
						<label>Estado</label>
						<input type="text" name="state" class="form-control" required>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>	
	</section>
@endsection