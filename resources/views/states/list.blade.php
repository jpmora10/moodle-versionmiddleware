@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'state/search', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
					<div class="form-group">
						<label>Estado</label>
						<input type="text" class="form-control" name="state">
					</div>
					<div class="form-control">
						<button type="submit" class="btn btn-default">Buscar</button>
						<a href="{{ route('state.index') }}" class="btn btn-primary">Todo</a>
						<a href="{{ route('state.create') }}" class="btn btn-primary">Crear</a>
					</div>	
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-condensed table-striped table-bordered">
					<thead>
						<tr>
							<th>Id</th>
							<th>Estado</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
					<?php
					      require 'config.php';
					      $states = $collection->find([]);
					      foreach($states as $state) {
					         echo "<tr>";
					         echo "<td>".$state->state."</td>";
					         echo "<td>";
					         echo "</td>";
					         echo "</tr>";
					      };
					?>
							<a class="btn btn-primary btn-xs" href="{{ route('state.edit',['id' => $state->id]) }}">Editar</a>
							<a class="btn btn-danger btn-xs" href="{{ route('state/destroy',['id' => $state->id]) }}">Eliminar</a>
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection