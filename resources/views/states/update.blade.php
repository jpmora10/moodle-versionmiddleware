@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($state,['route' => ['state.update', $state->id], 'method' => 'put', 'novalidate']) !!}
					<div class="form-group">
						<label>Estado</label>
						<input type="text" name="state" class="form-control" required value="{{ $state->state }}">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>	
	</section>
@endsection