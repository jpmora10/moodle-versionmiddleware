@extends('layouts.app')
@section('content')
	<new-movie :edit="true" :movie="{{$movie}}" :states="{{$states}}"></new-movie>
@endsection