@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-12">
				{!! Form::open(['route' => 'movie/search', 'method' => 'post', 'novalidate', 'class' => 'form-inline']) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" class="form-control" name="name">
					</div>
					<div class="form-control">
						<button type="submit" class="btn btn-default">Buscar</button>					
					</div>	
				{!! Form::close() !!}
			</article>
			<article class="col-md-12">
				<table class="table table-condensed table-striped table-bordered">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($movies as $movie)
							<tr>
								<td>{{ $movie->name }}</td>
								<td>{{ $movie->description }}</td>
								<td>
									@foreach($movie->categories as $movieCategory)
										{{ $movieCategory->name }}
									@endforeach
								</td>	
							</tr>
						@endforeach
					</tbody>
				</table>
			</article>
		</div>
	</section>
@endsection