@extends('layouts.app')
@section('content')
	<new-movie :edit="false" :movie="null" :states="null"></new-movie>
	<section>
    <div class="links">
    <form method="post" action="{{url('import-excel')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="file" name="excel">
        <br><br>
        <input type="submit" value="Enviar" style="padding: 10px 20px;">
    </form>
  </div>
  </section>
@endsection