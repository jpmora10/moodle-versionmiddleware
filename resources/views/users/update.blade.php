@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">
			<article class="col-md-10 col-md-offset-1">
				{!! Form::model($user,['route' => ['user.update', $user->id], 'method' => 'put', 'novalidate']) !!}
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="name" class="form-control" required value="{{ $user->name }}">
					</div>
					<div class="form-group">
						<label>E-mail</label>
						<input type="text" name="email" class="form-control" required value="{{ $user->email }}">
					</div>
					<div class="=form-group">	
						<p>Estado actual</p>
						<p>{{$user->state->state}}</p>
					</div>
					<div class="form-group">
						<select name="state" class="form-control">
							@forelse($states as $state)
	    						<option value="{{$state->id}}" >{{ $state->state }}</option>
							@empty
	    						<option>No hay estados registrados.</option>
							@endforelse
						</select>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>
					</div>
				{!! Form::close() !!}
			</article>
		</div>	
	</section>
@endsection